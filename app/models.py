
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Customer(models.Model):
	id = models.AutoField(primary_key=True)
	name = models.CharField(max_length=30,null=True, blank=True, default=None)
	address = models.CharField(max_length=250,null=True, blank=True, default=None)
	email = models.EmailField(max_length=250,blank=True, null= True, unique= True)
	phone = models.IntegerField()
	def __str__(self):
		return self.name
class Order(models.Model):
	order_id = models.AutoField(primary_key=True)
	customer_id = models.ForeignKey(Customer, on_delete=models.CASCADE)
	item = models.CharField(max_length=250,null=True, blank=True, default=None)
	price = models.FloatField(null=True, blank=True, default=None)

	def __str__(self):
		return self.item
